import json
import pika

from pika.exceptions import AMQPConnectionError
import django
import os
import sys

import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

# That code loads the Django settings so that you can use the email system.
# That function send_mail is the one that you'll use. Take a look at the Quick
# example on this page  to see how to use the send_mail function.


def process_approval(ch, method, properties, body):
    presentation_info = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f'{presentation_info["presenter_name"]}, we\'re happy to tell you that your presentation {presentation_info["title"]} has been accepted.',
        f"admin@conference.go",
        [f'{presentation_info["presenter_email"]}'],
        fail_silently=False,
    )
    print("mail sent")


def process_rejection(ch, method, properties, body):
    presentation_info = json.loads(body)
    send_mail(
        "Your presentation was not selected",
        f'{presentation_info["presenter_name"]}, we regret to tell you that your presentation {presentation_info["title"]} was not selected.',
        f"admin@conference.go",
        [f'{presentation_info["presenter_email"]}'],
        fail_silently=False,
    )
    print("mail sent")


# this api is called thus runnuning the entire process messeg def so whikle tru
# aor being accesed it will run code below based off this code

# def process_message(ch, method, properties, body ):
#     print("  Received %r" % body)


# parameters = pika.ConnectionParameters(host= 'rabbitmq')
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()
# channel.queue_declare(queue= 'tasks')
# channel.basic_consume(
#     queue= 'tasks',
#     on_message_callback=process_message,
#     auto_ack= True,
# )
# channel.start_consuming()


while True:
    try:
        print("hello thomas")
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        print(" [*] Waiting for messages. To exit press CTRL+C")
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
